<?php

/**
 * @file
 * Page callback file for the xmlrpc module.
 */

/**
 * Process an XML-RPC request.
 */
function xmlrpc_server_page() {
  module_load_include('inc', 'xmlrpc', 'xmlrpc');
  module_load_include('inc', 'xmlrpc', 'xmlrpcs');
  xmlrpc_server(module_invoke_all('xmlrpc'));
}
